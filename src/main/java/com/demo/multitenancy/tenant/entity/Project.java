package com.demo.multitenancy.tenant.entity;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "tbl_project", schema = "public")
public class Project implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "project_id")
    private Integer projectId;

    @Column(name = "name")
    private String name;

    @Column(name = "billable")
    private boolean billable;

    @Column(name = "utilized")
    private boolean utilized;

    @Column(name = "client_name")
    private String clientName;

    public Project(Integer projectId, String name, boolean billable, boolean utilized, String clientName) {
        this.projectId = projectId;
        this.name = name;
        this.billable = billable;
        this.utilized = utilized;
        this.clientName = clientName;
    }

    public Project() {
    }

    public Integer getProjectId() {
        return projectId;
    }

    public void setProjectId(Integer projectId) {
        this.projectId = projectId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isBillable() {
        return billable;
    }

    public void setBillable(boolean billable) {
        this.billable = billable;
    }

    public boolean isUtilized() {
        return utilized;
    }

    public void setUtilized(boolean utilized) {
        this.utilized = utilized;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }
}
