package com.demo.multitenancy.tenant.service;



import com.demo.multitenancy.tenant.entity.Project;

import java.util.List;

public interface ProjectService {
    List<Project> getAllProjects();

    Project getProjectById(int id);

    Project saveNewProject(Project project);

    Project editProject(Project project);

    void deleteProjectById(int id);

}