package com.demo.multitenancy.tenant.service;

import com.demo.multitenancy.tenant.entity.Project;
import com.demo.multitenancy.tenant.repository.ProjectRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProjectServiceImpl implements ProjectService {

    @Autowired
    ProjectRepository projectRepository;

    @Override
    public List<Project> getAllProjects() {
       return projectRepository.findAll();
    }

    @Override
    public Project getProjectById(int id) {
        Optional<Project> result = projectRepository.findById(id);
        Project proj = result.get();
        return  proj;
    }

    @Override
    public Project saveNewProject(Project project) {
        return projectRepository.save(project);
    }

    @Override
    public Project editProject(Project project) {
        Optional<Project> result = projectRepository.findById(project.getProjectId());
        Project source = result.get();
        source.setName(project.getName());
        source.setBillable(project.isBillable());
        source.setUtilized(project.isUtilized());
        source.setClientName(project.getClientName());

        return projectRepository.save(source);
    }

    @Override
    public void deleteProjectById(int id) {
        projectRepository.deleteById(id);
    }
}
