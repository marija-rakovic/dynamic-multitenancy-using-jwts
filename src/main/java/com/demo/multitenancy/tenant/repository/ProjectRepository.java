package com.demo.multitenancy.tenant.repository;

import com.demo.multitenancy.tenant.entity.Project;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProjectRepository extends JpaRepository<Project, Integer> {
}
