package com.demo.multitenancy.constant;

public enum UserStatus {
    ACTIVE, INACTIVE
}
