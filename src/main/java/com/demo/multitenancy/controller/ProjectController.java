package com.demo.multitenancy.controller;

import com.demo.multitenancy.security.RequestAuthorization;
import com.demo.multitenancy.tenant.entity.Project;
import com.demo.multitenancy.tenant.service.ProjectService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;

@RestController
@RequestMapping("/api/project")
public class ProjectController implements Serializable {

    private static final Logger LOGGER = LoggerFactory.getLogger(AuthenticationController.class);

    @Autowired
    ProjectService projectService;

    @RequestAuthorization
    @GetMapping("/")
    public ResponseEntity<Object> getAllProjects() {
        LOGGER.info("getAllProjects() api call");
        return new ResponseEntity<>(projectService.getAllProjects(), HttpStatus.OK);
    }

    @RequestAuthorization
    @GetMapping("/{id}")
    public Project getById(@PathVariable("id") int id) {
        return projectService.getProjectById(id);
    }

    @RequestAuthorization
    @PostMapping("/")
    public Project saveProject(@RequestBody Project project) {
        Project result = projectService.saveNewProject(project);
        return result;
    }

    @PutMapping("/")
    public Project editProject(@RequestBody Project project) {
        Project result = projectService.saveNewProject(project);
        return result;
    }

    @DeleteMapping("/{id}")
    public void deleteProject(@PathVariable("id") int id) {
        projectService.deleteProjectById(id);
    }

}
