package com.demo.multitenancy.mastertenant.service;

import com.demo.multitenancy.mastertenant.entity.MasterTenant;


public interface MasterTenantService {

    MasterTenant findByClientId(Integer clientId);
}
