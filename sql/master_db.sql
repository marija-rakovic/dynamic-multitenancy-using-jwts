--
-- PostgreSQL database dump
--

-- Dumped from database version 10.14 (Ubuntu 10.14-1.pgdg18.04+1)
-- Dumped by pg_dump version 12.4 (Ubuntu 12.4-1.pgdg18.04+1)

-- Started on 2020-10-21 14:25:45 CEST

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

--
-- TOC entry 196 (class 1259 OID 51322)
-- Name: tenant_master; Type: TABLE; Schema: public; Owner: marija
--

CREATE TABLE public.tenant_master (
    tenant_client_id bigint NOT NULL,
    db_name character varying(50) NOT NULL,
    url character varying(100) NOT NULL,
    user_name character varying(50) NOT NULL,
    password character varying(100) NOT NULL,
    driver_class character varying(100) NOT NULL,
    status character varying(10) NOT NULL
);


ALTER TABLE public.tenant_master OWNER TO marija;

--
-- TOC entry 2907 (class 0 OID 51322)
-- Dependencies: 196
-- Data for Name: tenant_master; Type: TABLE DATA; Schema: public; Owner: marija
--

COPY public.tenant_master (tenant_client_id, db_name, url, user_name, password, driver_class, status) FROM stdin;
100	test_db1	jdbc:postgresql://localhost:5432/test_db1	marija	password	org.postgresql.Driver	Active
200	test_db2	jdbc:postgresql://localhost:5432/test_db2	marija	password	org.postgresql.Driver	Active
\.


--
-- TOC entry 2785 (class 2606 OID 51326)
-- Name: tenant_master tenant_master_pkey; Type: CONSTRAINT; Schema: public; Owner: marija
--

ALTER TABLE ONLY public.tenant_master
    ADD CONSTRAINT tenant_master_pkey PRIMARY KEY (tenant_client_id);


-- Completed on 2020-10-21 14:25:45 CEST

--
-- PostgreSQL database dump complete
--

