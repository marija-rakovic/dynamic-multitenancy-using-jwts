--
-- PostgreSQL database dump
--

-- Dumped from database version 10.14 (Ubuntu 10.14-1.pgdg18.04+1)
-- Dumped by pg_dump version 12.4 (Ubuntu 12.4-1.pgdg18.04+1)

-- Started on 2020-10-21 14:27:06 CEST

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

--
-- TOC entry 199 (class 1259 OID 51899)
-- Name: tbl_project; Type: TABLE; Schema: public; Owner: marija
--

CREATE TABLE public.tbl_project (
    project_id bigint NOT NULL,
    name character varying(100) NOT NULL,
    billable boolean NOT NULL,
    utilized boolean NOT NULL,
    client_name character varying(100) NOT NULL
);


ALTER TABLE public.tbl_project OWNER TO marija;

--
-- TOC entry 198 (class 1259 OID 51897)
-- Name: tbl_project_project_id_seq; Type: SEQUENCE; Schema: public; Owner: marija
--

ALTER TABLE public.tbl_project ALTER COLUMN project_id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.tbl_project_project_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 197 (class 1259 OID 51330)
-- Name: tbl_user; Type: TABLE; Schema: public; Owner: marija
--

CREATE TABLE public.tbl_user (
    user_id bigint NOT NULL,
    full_name character varying(100) NOT NULL,
    gender character varying(10) NOT NULL,
    user_name character varying(50) NOT NULL,
    password character varying(100) NOT NULL,
    status character varying(10) NOT NULL
);


ALTER TABLE public.tbl_user OWNER TO marija;

--
-- TOC entry 196 (class 1259 OID 51328)
-- Name: tbl_user_user_id_seq; Type: SEQUENCE; Schema: public; Owner: marija
--

ALTER TABLE public.tbl_user ALTER COLUMN user_id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.tbl_user_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 2920 (class 0 OID 51899)
-- Dependencies: 199
-- Data for Name: tbl_project; Type: TABLE DATA; Schema: public; Owner: marija
--

COPY public.tbl_project (project_id, name, billable, utilized, client_name) FROM stdin;
1	Prvi Projekat	t	t	RCMT
2	Drugi Projekat	t	t	PSR
\.


--
-- TOC entry 2918 (class 0 OID 51330)
-- Dependencies: 197
-- Data for Name: tbl_user; Type: TABLE DATA; Schema: public; Owner: marija
--

COPY public.tbl_user (user_id, full_name, gender, user_name, password, status) FROM stdin;
3	Marija Admin	female	marija	$2b$10$cZmdTRErvcwjbc8vNWREo.B/DCw/pRmMeDGKzrc6SrjhDTgPo0L8i	Active
\.


--
-- TOC entry 2926 (class 0 OID 0)
-- Dependencies: 198
-- Name: tbl_project_project_id_seq; Type: SEQUENCE SET; Schema: public; Owner: marija
--

SELECT pg_catalog.setval('public.tbl_project_project_id_seq', 3, true);


--
-- TOC entry 2927 (class 0 OID 0)
-- Dependencies: 196
-- Name: tbl_user_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: marija
--

SELECT pg_catalog.setval('public.tbl_user_user_id_seq', 3, true);


--
-- TOC entry 2795 (class 2606 OID 51903)
-- Name: tbl_project tbl_project_pkey; Type: CONSTRAINT; Schema: public; Owner: marija
--

ALTER TABLE ONLY public.tbl_project
    ADD CONSTRAINT tbl_project_pkey PRIMARY KEY (project_id);


--
-- TOC entry 2793 (class 2606 OID 51334)
-- Name: tbl_user tbl_user_pkey; Type: CONSTRAINT; Schema: public; Owner: marija
--

ALTER TABLE ONLY public.tbl_user
    ADD CONSTRAINT tbl_user_pkey PRIMARY KEY (user_id);


-- Completed on 2020-10-21 14:27:06 CEST

--
-- PostgreSQL database dump complete
--

